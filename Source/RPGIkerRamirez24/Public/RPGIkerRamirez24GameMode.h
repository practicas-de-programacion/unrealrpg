// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RPGIkerRamirez24GameMode.generated.h"

UCLASS(minimalapi)
class ARPGIkerRamirez24GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ARPGIkerRamirez24GameMode();
};



