// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Templates/SubclassOf.h"
#include "GameFramework/PlayerController.h"
#include "InputActionValue.h"
#include "SkillDataRow.h"
#include "StatDataRow.h"
#include "RPGIkerRamirez24PlayerController.generated.h"

class UNiagaraSystem;
class UInputMappingContext;
class UInputConfigData;
class ANpcChar;

class ARPGIkerRamirez24Character;


DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnLocationClick, FVector, aClickLocation, const FSkillDataRow&, aSkillData);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnDirectionSkillCast, FRotator, aRotation, FSkillDataRow, aSkillData);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnLocationSkillCast, FVector, aClickLocation, FSkillDataRow, aSkillData);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnActorClick, AActor*, aActorLocation, const FSkillDataRow&, aSkillData);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnActorSkillCast, AActor*, aActor, FSkillDataRow, aSkillData);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnActionTalk, ANpcChar*, aNPC);

UCLASS()
class ARPGIkerRamirez24PlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ARPGIkerRamirez24PlayerController();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		ARPGIkerRamirez24Character* mpCharacter;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
		float ShortPressThreshold;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
		UNiagaraSystem* FXCursor;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
		UInputMappingContext* DefaultMappingContext;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category=Input)
		UInputConfigData* mInputData;

#pragma region SKILLS
	
	FSkillDataRow* GetSkill(ESkill skill);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=mSKILL)
		TArray<ESkill> mSkills;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=mSKILL)
		UDataTable* mSkillDB;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=mSKILL)
		FSkillDataRow mSkillSelected;
	
#pragma endregion 	

#pragma region STATS
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=mSTATS)
		UDataTable* mStatsDB;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=mSTATS)
		EClassType mClass;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category=mSTATS)
		float CurrentHP;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category=mSTATS)
		float Strenght;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category=mSTATS)
		float Inteligence;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category=mSTATS)
		float Velocity;
	
#pragma endregion 
	
#pragma region EVENTS

	UPROPERTY(BlueprintAssignable, BlueprintCallable)
	
		FOnLocationClick evOnLocationClick;

	UPROPERTY(BlueprintAssignable, BlueprintCallable)
		FOnActorClick evOnActorClick;

	UPROPERTY(BlueprintAssignable, BlueprintCallable)
		FOnDirectionSkillCast evOnDirectionSkillCast;
	
	UPROPERTY(BlueprintAssignable, BlueprintCallable)
		FOnLocationSkillCast evOnLocationSkillCast;
	
	UPROPERTY(BlueprintAssignable, BlueprintCallable)
		FOnActorSkillCast evOnActorSkillCast;

	UPROPERTY(BlueprintAssignable, BlueprintCallable)
		FOnActionTalk evOnActionTalk;
	
#pragma endregion 
	
protected:
	uint32 bMoveToMouseCursor : 1;

	virtual void SetupInputComponent() override;
	virtual void BeginPlay();

	void OnInputStarted();
	void OnSetDestinationTriggered();
	void OnSetDestinationReleased();
	void OnActionStarted();
	
	void OnSkillPressed(int aButtonPressed);
	void OnActPressed();

	void GetClassData();
	
private:
	FHitResult mHitResult;
	FVector CachedDestination;
	AActor* Actor;
	bool bIsTouch; // Is it a touch device
	float FollowTime; // For how long it has been pressed
	
};


