
#include "NpcChar.h"
#include "RPGIkerRamirez24/Public/Utils.h"

ANpcChar::ANpcChar()
{
 	PrimaryActorTick.bCanEverTick = true;
}

void ANpcChar::BeginPlay()
{
	Super::BeginPlay();
	//ScreenD("Me he iniciado");
}

void ANpcChar::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

FString ANpcChar::getConversation()
{
	return myText;
}

FColor ANpcChar::getConversationColor()
{
	return myTextColor;
}
// Called to bind functionality to input
void ANpcChar::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

