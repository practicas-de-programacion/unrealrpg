// Copyright Epic Games, Inc. All Rights Reserved.

#include "RPGIkerRamirez24PlayerController.h"
#include "GameFramework/Pawn.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "NiagaraSystem.h"
#include "NiagaraFunctionLibrary.h"
#include "RPGIkerRamirez24Character.h"
#include "Engine/World.h"
#include "EnhancedInputComponent.h"
#include "InputConfigData.h"
#include "EnhancedInputSubsystems.h"
#include "NpcChar.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "RPGIkerRamirez24/Public/Utils.h"

ARPGIkerRamirez24PlayerController::ARPGIkerRamirez24PlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Default;
	CachedDestination = FVector::ZeroVector;
	FollowTime = 0.f;
}

void ARPGIkerRamirez24PlayerController::BeginPlay()
{
	[[maybe_unused]] auto cosa {GetCharacter()};
	mpCharacter = Cast<ARPGIkerRamirez24Character>(cosa);
	
	GetClassData();
	
	Super::BeginPlay();
	
	if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer()))
	{
		Subsystem->AddMappingContext(DefaultMappingContext, 0);
	}
}

void ARPGIkerRamirez24PlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	
	UEnhancedInputLocalPlayerSubsystem* EInputSubsistem = ULocalPlayer :: GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer());
	EInputSubsistem->ClearAllMappings();
	EInputSubsistem->AddMappingContext(DefaultMappingContext, 0);

	UEnhancedInputComponent* EInputComponent {Cast<UEnhancedInputComponent>(InputComponent)};
	
	EInputComponent->BindAction(mInputData->mpInputMove, ETriggerEvent::Started, this, &ARPGIkerRamirez24PlayerController::OnInputStarted);
	EInputComponent->BindAction(mInputData->mpInputMove, ETriggerEvent::Triggered, this, &ARPGIkerRamirez24PlayerController::OnSetDestinationTriggered);
	EInputComponent->BindAction(mInputData->mpInputMove, ETriggerEvent::Completed, this, &ARPGIkerRamirez24PlayerController::OnSetDestinationReleased);

	EInputComponent->BindAction(mInputData->mpInputAct, ETriggerEvent::Started, this, &ARPGIkerRamirez24PlayerController::OnActionStarted);
	
	EInputComponent->BindAction(mInputData->mpInputStop, ETriggerEvent::Triggered, this, &ARPGIkerRamirez24PlayerController::StopMovement);
	
	for (int ButtonIndex = 0; ButtonIndex < mInputData->mpInputSkills.Num(); ++ButtonIndex)
	{
		EInputComponent->BindAction(mInputData->mpInputSkills[ButtonIndex], ETriggerEvent::Completed, this, &ARPGIkerRamirez24PlayerController::OnSkillPressed, ButtonIndex);
	}
}

void ARPGIkerRamirez24PlayerController::OnActionStarted()
{
	GetHitResultUnderCursorByChannel(TraceTypeQuery1, true, mHitResult);

	if(auto* npcChar{Cast<ANpcChar>(mHitResult.GetActor())}; npcChar != nullptr)
	{
		evOnActionTalk.Broadcast(npcChar);
	}
}

void ARPGIkerRamirez24PlayerController::OnInputStarted()
{
	StopMovement();
}

void ARPGIkerRamirez24PlayerController::OnSetDestinationTriggered()
{
	FollowTime += GetWorld()->GetDeltaSeconds();
	
	bool bHitSuccessful = GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, true, mHitResult);
	
	if (bHitSuccessful)
	{
		CachedDestination = mHitResult.Location;
	}
	
	APawn* ControlledPawn = GetPawn();
	if (ControlledPawn != nullptr)
	{
		FVector WorldDirection = (CachedDestination - ControlledPawn->GetActorLocation()).GetSafeNormal();
		ControlledPawn->AddMovementInput(WorldDirection, 1.0, false);
	}
}

void ARPGIkerRamirez24PlayerController::OnSetDestinationReleased()
{
	if (FollowTime <= ShortPressThreshold)
	{
		UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, CachedDestination);
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, FXCursor, CachedDestination, FRotator::ZeroRotator, FVector(1.f, 1.f, 1.f), true, true, ENCPoolMethod::None, true);
	}
	FollowTime = 0.f;
}

void ARPGIkerRamirez24PlayerController::OnSkillPressed(int aButtonPressed)
{
	if(auto SkillPressed {mSkills[aButtonPressed]}; SkillPressed != ESkill::NONE)
	{
		if(auto* Skill {GetSkill(SkillPressed)}; Skill)
		{
			//ScreenD("bbbbbbbbbbbbbbb");
			//ScreenD(Format1("Skill: %s", *Skill->Description));	
			mSkillSelected = *Skill;
			OnActPressed();
		}
	}
}

FSkillDataRow* ARPGIkerRamirez24PlayerController::GetSkill(ESkill aSkill)
{

	FSkillDataRow* SkillFound {};

	if(mSkillDB)
	{
		FName SkillString{UEnum::GetDisplayValueAsText(aSkill).ToString()};

		static const FString FindContext {FString("Searching for ").Append(SkillString.ToString())};
		
		SkillFound = mSkillDB->FindRow<FSkillDataRow>(SkillString, FindContext, true);
	}
	
	return SkillFound;
}

void ARPGIkerRamirez24PlayerController::OnActPressed()
{
	if(mSkillSelected.Name == ESkill::NONE) return;

	FVector HitLocation {FVector::ZeroVector};
	GetHitResultUnderCursorByChannel(TraceTypeQuery1, true, mHitResult);

	const bool hasActor {mHitResult.GetActor() != nullptr};
	const bool canBeDamaged {mHitResult.GetActor()->CanBeDamaged()};
	//ScreenD(Format1("%d nulor", hasActor));
	//ScreenD(Format1("%d dama", canBeDamaged));
	//ScreenD(Format1("%s actor", *mHitResult.GetActor()->GetName()));
	if(hasActor && canBeDamaged)
	{
		evOnActorClick.Broadcast(mHitResult.GetActor(), mSkillSelected);
	}
	else
	{
		evOnLocationClick.Broadcast(mHitResult.Location, mSkillSelected);
	}
}

void ARPGIkerRamirez24PlayerController::GetClassData()
{
	if(mStatsDB != nullptr && mClass != EClassType::NONE)
	{
		FName StatString{UEnum::GetDisplayValueAsText(mClass).ToString()};
		static const FString FindContext {FString("Searching for class ").Append(StatString.ToString())};
		auto ClassFound = mStatsDB->FindRow<FStatDataRow>(StatString, FindContext, true);

		if(ClassFound != nullptr)
		{
			CurrentHP = ClassFound->Health;
            Strenght = ClassFound->Damage;
            Inteligence = ClassFound->Inteligence;
            Velocity = ClassFound->Velocity;
    
            mpCharacter->GetCharacterMovement()->MaxWalkSpeed = 100*Velocity;
		}
	}
}

