// Copyright Epic Games, Inc. All Rights Reserved.

#include "RPGIkerRamirez24GameMode.h"
#include "RPGIkerRamirez24PlayerController.h"
#include "RPGIkerRamirez24Character.h"
#include "UObject/ConstructorHelpers.h"

ARPGIkerRamirez24GameMode::ARPGIkerRamirez24GameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ARPGIkerRamirez24PlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// set default controller to our Blueprinted controller
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownPlayerController"));
	if(PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
}