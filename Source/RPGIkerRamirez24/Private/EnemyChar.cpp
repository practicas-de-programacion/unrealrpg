
#include "EnemyChar.h"

AEnemyChar::AEnemyChar()
{
 	PrimaryActorTick.bCanEverTick = true;

}

void AEnemyChar::BeginPlay()
{
	Super::BeginPlay();
	
	OnTakeAnyDamage.AddDynamic(this, &AEnemyChar::OnHitCallback);
	
}

void AEnemyChar::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AEnemyChar::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AEnemyChar::OnHitCallback(AActor* apDamagedActor, float aDamage, const UDamageType* apDamageType,
	AController* apInstigatedBy, AActor* apDamageCauser)
{

	mHp -= aDamage;
	if(mHp <= 0)
	{
		Destroy();
	}

	
}