// Copyright Epic Games, Inc. All Rights Reserved.

#include "RPGIkerRamirez24.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, RPGIkerRamirez24, "RPGIkerRamirez24" );

DEFINE_LOG_CATEGORY(LogRPGIkerRamirez24)
 