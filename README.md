# Risketos Basicos:

- [X] Sistema para clases
- [X] Sistema de combate por habilidades
- [X] Enemigos
- [X] Uso de UMacros
- [X] Sistema de habilidades

# Risketos Opcionales:

- [X] Fracture (Fracturar los fps)
- [X] NPC con dialogos
- [X] Rave (rave (rave))
- [X] Iluminacion (Volume)
- [X] IA Enemigos BehaviourTree
- [X] HUD

# Controles:
- rmc - hablar
- lmc - moverse
- qwer - habilidades
